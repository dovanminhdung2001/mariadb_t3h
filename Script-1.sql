USE classicmodels

-- 2 liệt kê offices có country là USA
SELECT *
FROM offices o
WHERE country LIKE 'USA'

-- 3 liệt kê tất cả employess, thêm cột alias fullName, bằng firstname + lastname
SELECT e.employeeNumber , e.firstName, e.lastName , concat(e.firstName, ' ', e.lastName) AS fullName
FROM employees e

-- 4 liệt kê tất cả employess, thêm cột alias offices.city, office.state
SELECT e.*, o.city AS offices_city, o.state AS offices_state
FROM employees e , offices o
WHERE e.officeCode LIKE o.officeCode

-- 5 liệt kê offices ko chứa employee nào 
SELECT *
FROM offices o
WHERE EXISTS (SELECT DISTINCT e.officeCode
FROM employees e)

-- 6 đến số lương employees
SELECT count(e.employeeNumber) AS 'Số employee'
FROM employees e

-- 7 liệt kê customers có nhiều nhất 3 payments
SELECT c.*
FROM customers c
WHERE c.customerNumber NOT IN (SELECT DISTINCT p.customerNumber
FROM payments p)
OR
	  	c.customerNumber IN (SELECT c2.customerNumber
FROM customers c2, payments p2
WHERE c2.customerNumber = p2.customerNumber
GROUP BY c2.customerNumber
HAVING count(p2.checkNumber) < 4) 

SELECT c.customerNumber, count(p.customerNumber) AS 'num of payment'
FROM customers c
LEFT JOIN payments p 
	ON
(c.customerNumber = p.customerNumber)
GROUP BY c.customerNumber
HAVING count(p.customerNumber) < 4

-- 8 liệt kê 10 customer có payments sớm nhất  
CREATE TABLE tbl1 AS
SELECT p.customerNumber
FROM payments p
ORDER BY p.paymentDate  

SELECT DISTINCT *
FROM tbl1
LIMIT 10

CREATE VIEW vw1 AS
SELECT p.customerNumber
FROM payments p
ORDER BY p.paymentDate

SELECT *
FROM tbl1 

SELECT *
FROM vw1

SELECT DISTINCT *
FROM tbl1  

SELECT DISTINCT *
FROM vw1

-- 9 liệt kê các customers có salesRepEmployeeNumber tồn tại
SELECT *
FROM customers c
WHERE NOT isnull(c.salesRepEmployeeNumber)

-- 10. liệt kê 10 customer có nhiều payment nhất, sắp xếp giảm dần số lượng payment
SELECT customerNumber, count(checkNumber) AS 'num of payment'
FROM payments p
GROUP BY customerNumber
ORDER BY count(checkNumber) DESC
LIMIT 10

-- 11. tìm tất cả orders trong tháng 11/2003
SELECT *
FROM orders o
WHERE orderDate LIKE '2003-11%'

-- 12 thêm 2 employee 
INSERT INTO employees 
(employeeNumber, lastName, firstName, extension, email, officeCode, reportsTo, jobTitle)
VALUES
(1, 'a' , 'a' , 'a' , 'a@' , '1' , 1, 'a'),
(2, 'b', 'b', 'b', 'b@', '2', 2, 'b');

SELECT *
FROM offices o

-- 13 thêm 2 office
-- 14 sửa addressLine2 = '31 street Red'  của employee có id 172
-- (customer thi hop ly hon)
UPDATE employees c
SET
addressLine2 = '31 streeet Red'
WHERE employeeNumber = 172

-- 15 update tất cả customer có addressLine là Null bằng addressLine1
CREATE VIEW old_customers AS 
SELECT *
FROM customers c2 

SELECT *
FROM old_customers oc 

UPDATE customers
SET
addressLine2 = addressLine1
WHERE isnull(addressLine2) 

SELECT *
FROM customers c

-- 16 update tất cả customer có state là Null bằng 3 kí tự dầu của city, viết hoa chữ cái đầu
UPDATE customers
SET
state = concat(upper(substr(city, 1, 1)), substr(city, 2, 2))
WHERE isnull(state) 

SELECT *
FROM customers c

-- 17 liệt kê 10 customer có nhiều orders nhất 
SELECT customerNumber , count(orderNumber) AS 'num of order'
FROM orders o
GROUP BY customerNumber
ORDER BY count(orderNumber) DESC
LIMIT 10

-- 18 liệt kê tất cả orderdetails của từng orders
SELECT *
FROM orders o , orderdetails o2
WHERE o.orderNumber = o2.orderNumber

-- 19 liệt kê tất cả orderdetails, order có productCode bắt đầu là S10
SELECT *
FROM orders o , orderdetails o2
WHERE o.orderNumber = o2.orderNumber
AND o2.productCode LIKE 'S10%'

-- 20 liệt kê 11 product có ít orderdetails nhâṭ
SELECT p.productCode , count(o.productCode) AS 'num of order'
FROM products p
LEFT JOIN orderdetails o 
	ON
p.productCode = o.productCode
GROUP BY p.productCode
ORDER BY count(o.productCode)
LIMIT 11

-- 21 liệt kê thông tin productlines, products, orderdetails của từng order
SELECT *
FROM orders o , orderdetails o2 , products p , productlines p2
WHERE o.orderNumber = o2.orderNumber
AND o2.productCode = p.productCode
AND p.productLine = p2.productLine
-- 22 thêm 3 products
-- 23 xóa customers k có bất kì order nào
